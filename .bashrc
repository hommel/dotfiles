# ~/.bashrc: executed by bash(1) for non-login shells.

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# source all files in ~/.bashrc.d/
for file in ~/.bashrc.d/*; do
    [ -f "$file" ] || continue
    . "$file"
done
