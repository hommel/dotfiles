#!/bin/bash -e
# Script to install Firefox Developer Edition

getlatest() {
    # Output the URL of the latest version of Firefox dev edition
    curl \
        --output /dev/null \
        --silent \
        --show-error \
        -H "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0" \
        -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" \
        -H "Accept-Encoding: gzip, deflate, br" \
        -H "Referer: https://www.mozilla.org/" \
        -w "%{redirect_url}\n" \
        https://download.mozilla.org/?product=firefox-devedition-latest-ssl\&os=linux64\&lang=en-US
}

url=$(getlatest)
# this is a bit precarious relying on the 7th field, but it works well enough
version=$(echo $url | cut -d"/" -f7)
tarball=$(echo $url | cut -d"/" -f10)
echo "latest available version is $version."

tdir=/tmp
ldir=$HOME/lib
bdir=$HOME/bin
mkdir -p $ldir && mkdir -p $bdir
base=$ldir/firefox

# Check if version of firefox is installed
if [[ ! -d $base/$version ]]; then
    echo "firefox version $version not installed."
    # Check if the tarball is present in the /tmp directory
    if ! stat $tdir/$tarball &> /dev/null; then
        echo "firefox tarball $tarball not downloaded."
        echo "downloading..."
        # Download the tarball
        wget -O $tdir/$tarball $url
    fi
    echo "firefox tarball $tarball downloaded"
    echo "installing..."
    # make sure the base directory is present
    mkdir -p $base
    # create a release directory
    mkdir -p $base/$version
    # unpack tarball into release directory
    tar xjf $tdir/$tarball -C $base/$version
fi
echo "firefox version $version installed"

# create a symlink to the version (outside the if-statement for idempotence)
ln -sf $base/$version/firefox/firefox $bdir/

# make sure dependencies are installed
#
# there are certainly more but it's a challenge to determine due to other
# package dependencies (e.g. virtualbox)
# sudo apt-get install -y \
#     libdbus-glib-1-2

# # install extensions to global directory
# edir=$base/$version/firefox/browser/extensions
# mkdir -p $edir

# # LastPass
# target=https://lastpass.com/lastpassffx/xpi.php
# rename=support@lastpass.com.xpi
# if ! stat $edir/$rename &> /dev/null; then
#     wget -O $edir/$rename $target
# fi
# # # also copy the binary version of the browser extension into firefox lib
# # cp libnplastpass64.so $base/$version/firefox

# # uBlock Origin (follow the redirect to get latest release tag)
# #latest=https://github.com/gorhill/uBlock/releases/latest
# #tagged=$(curl -ILs -w %{url_effective} -o /dev/null $latest)
# #tag=$(echo $tagged | awk -F/ '{print $NF}')
# #
# # hardcoding for now until release asset naming gets under control
# tag=1.16.2
# xpi=uBlock0.webext.xpi
# target="https://github.com/gorhill/uBlock/releases/download/$tag/$xpi"
# rename=uBlock0@raymondhill.net.xpi
# # this should fail if the file is not downloaded.  instead it saves an
# # empty file as `rename` which cases subsequent script runs to assume it
# # has been installed.
# if ! stat $edir/$rename &> /dev/null; then
#     wget -O $edir/$rename $target
# fi

# # HTTPS Everywhere
# target=https://www.eff.org/files/https-everywhere-latest.xpi
# rename=https-everywhere-eff@eff.org.xpi
# if ! stat $edir/$rename &> /dev/null; then
#     wget -O $edir/$rename $target
# fi
