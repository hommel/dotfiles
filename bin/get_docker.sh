#!/bin/bash -e
# Script to install docker-cd and docker-compose

# Run as root in case installation preceeds presence of sudo.
if ! [ $(id -u) = 0 ]; then
   echo "Must be run as root. Exiting."
   exit 1
fi

# Install packages to allow apt to use a repository over HTTPS.
apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common

# Add Docker’s official GPG key.
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

# Set up the stable repository.
add-apt-repository \
    "deb [arch=amd64] https://download.docker.com/linux/debian \
    $(lsb_release -cs) \
    stable"

# Update the apt package index and install the latest version of Docker CE
sudo apt-get update && apt-get install -y docker-ce
